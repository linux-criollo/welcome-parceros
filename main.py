import os
import subprocess
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib


class Welcome:

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('templates/guelcom.glade')
        self.window = self.builder.get_object("TETA OS")
        self.label_bienvenida = self.builder.get_object("label_bienvenida")
        self.label_herramientas = self.builder.get_object("label_herramientas")
        self.label_soporte = self.builder.get_object("label_soporte")
        self.label_kernel = self.builder.get_object("label_kernel")
        self.label_instalacion = self.builder.get_object("label_instalacion")
        self.boton_instalar = self.builder.get_object("Instalar")
        self.window.set_title("Bienvenido")
        self.window.set_position(Gtk.WindowPosition.CENTER)
        self.window.show()
        self.window.connect('destroy',self.onDestroy)
        self.boton_instalar.connect('pressed',self.run_installer)

        with open("templates/markup_labels/Welcome.txt", "r") as markup_labels:
            labels = markup_labels.read()
        self.label_bienvenida.set_markup(labels)
        
        with open("templates/markup_labels/Herramientas.txt", "r") as markup_labels:
            labels = markup_labels.read()
        self.label_herramientas.set_markup(labels)

        with open("templates/markup_labels/Soporte.txt", "r") as markup_labels:
            labels = markup_labels.read()
        self.label_soporte.set_markup(labels)

        with open("templates/markup_labels/Kernel.txt", "r") as markup_labels:
            labels = markup_labels.read()
        self.label_kernel.set_markup(labels)

        with open("templates/markup_labels/Instalacion.txt", "r") as markup_labels:
            labels = markup_labels.read()
        self.label_instalacion.set_markup(labels)

    def onDestroy(self, *args):
        Gtk.main_quit()

    def run_installer(self, button):
        try:
            subprocess.Popen(["/opt/AddDelUser/RUN"])
            Gtk.main_quit()
        except subprocess.CalledProcessError as e:
            print(e.output)


if __name__ == "__main__":
    assistant = Welcome()
    Gtk.main()